package ru.mozomig.changemoney.core.utils

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import ru.mozomig.changemoney.core.App
import ru.mozomig.changemoney.model.repositories.CurrencyRepository
import ru.mozomig.changemoney.model.repositories.CurrencyRepositoryImpl
import ru.mozomig.changemoney.model.services.CurrencyService

val repoModule = Kodein.Module("repository") {
    bind<CurrencyRepository>() with singleton { CurrencyRepositoryImpl(App.INSTANCE) }
}

val apiModule = Kodein.Module("api") {
    bind() from singleton {
        Retrofit.Builder()
            .baseUrl("https://www.ecb.europa.eu/")
            .addConverterFactory(SimpleXmlConverterFactory.create() )
            .build()
    }

    bind<CurrencyService>() with singleton { instance<Retrofit>().create(CurrencyService::class.java)}
}