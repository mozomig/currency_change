package ru.mozomig.changemoney.core.utils

import android.content.Context
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import java.math.BigDecimal
import kotlin.math.roundToInt

inline fun <reified T : ViewModel> FragmentActivity.getViewModel() = lazy {
    ViewModelProviders.of(this).get(T::class.java)
}

inline fun <reified T : ViewModel> Fragment.getViewModel() = lazy {
    ViewModelProviders.of(this).get(T::class.java)
}

inline fun Context.dp2px(dp: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).roundToInt()

inline fun Context.dp2px(dp: Int) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).roundToInt()

inline fun View.dp2px(dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
inline fun View.dp2px(dp: Int) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).roundToInt()

inline fun String.parseBigDecimal(): BigDecimal? = try {
    BigDecimal(this.removeNotDigits(this.normalizeAmountValue("")))
} catch (e: Exception) {
    e.printStackTrace()
    null
}

inline fun String.removeNotDigits(string: String): String {
    return if (TextUtils.isEmpty(string)) string else string.replace("[^\\+0-9.-]".toRegex(), "")
}

inline fun String.normalizeAmountValue(fallback: String): String {
    return if (android.text.TextUtils.isEmpty(this)) {
        fallback
    } else this.trim { it <= ' ' }.replace("\\s".toRegex(), "").replace(',', '.')
}

inline fun String.toSafeBigDecimal() = try {
    normalizeAmountValue("").toBigDecimal()
} catch (e: NumberFormatException) {
    BigDecimal(0)
}

