package ru.mozomig.changemoney.core

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import ru.mozomig.changemoney.core.utils.apiModule
import ru.mozomig.changemoney.core.utils.repoModule

class App: Application(), KodeinAware {

    companion object {
        lateinit var INSTANCE: Application
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    override val kodein: Kodein = Kodein.lazy {
        import(repoModule)
        import(apiModule)
    }


}