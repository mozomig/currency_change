package ru.mozomig.changemoney.core.utils

import android.os.Build
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.textfield.TextInputLayout
import org.jetbrains.anko.backgroundColor
import ru.mozomig.changemoney.R

inline fun Toolbar.default() {
    backgroundColor = correctColor(android.R.color.white)
}

inline fun TextInputLayout.defaultStyle() {
    setBoxBackgroundMode(TextInputLayout.BOX_BACKGROUND_FILLED)
}