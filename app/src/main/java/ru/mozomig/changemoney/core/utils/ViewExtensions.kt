package ru.mozomig.changemoney.core.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.util.TypedValue
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible


inline fun Context.correctColor(id: Int) = resources.correctColor(id)

inline fun View.correctColor(id: Int) = resources.correctColor(id)

inline fun Resources.correctColor(id: Int) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    getColor(id, null)
} else {
    getColor(id)
}

inline fun Context.correctDrawable(id: Int) = resources.correctDrawable(id)

inline fun View.correctDrawable(id: Int) = resources.correctDrawable(id)

inline fun Resources.correctDrawable(id: Int) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    getDrawable(id, null)
} else {
    getDrawable(id)
}

inline fun Context.correctStateList(id: Int) = resources.correctStateList(id)

inline fun View.correctStateList(id: Int) = resources.correctStateList(id)

inline fun Resources.correctStateList(id: Int) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    getColorStateList(id, null)
} else {
    getColorStateList(id)
}

inline fun View.getAttr(attrId: Int) = context.getAttr(attrId)

inline fun Context.getAttr(attrId: Int) = with(TypedValue()) {
    if (theme?.resolveAttribute(attrId, this, true) == true)
        TypedValue.complexToDimensionPixelSize(this.data, resources.displayMetrics)
    else 0
}

inline fun View.getAttrResourceId(attrId: Int) = context.getAttrResourceId(attrId)

inline fun Context.getAttrResourceId(attrId: Int) = with(TypedValue()) {
    if (theme?.resolveAttribute(attrId, this, true) == true)
        resourceId
    else 0
}

inline fun View.visibility(show: Boolean) {
    visibility = when (show) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}

inline fun View.gone() {
    visibility = View.GONE
}

inline fun View.visible() {
    visibility = View.VISIBLE
}

inline fun View.show(show: Boolean) {
    takeIf { show }?.let { visible() } ?: gone()
}

inline fun View.fadeShow(show: Boolean) {
    takeIf { show }?.let { fadeVisible() } ?: fadeGone()
}

inline fun View.inRoLShow(show: Boolean) {
    takeIf { show }?.let { inFromRight() } ?: outToRight()
}

fun View.fadeGone(duration: Long = 300) {
    animate()
        .alpha(0.0f)
        .setDuration(duration)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                this@fadeGone.visibility = View.GONE
            }
        })
}

fun View.fadeVisible(duration: Long = 300) {
    animate()
        .alpha(1.0f)
        .setDuration(duration)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationStart(animation)
                this@fadeVisible.visibility = View.VISIBLE
                this@fadeVisible.alpha = 0f
            }
        })
}

fun View.inFromRight(duration: Long = 300) {
    takeIf { isGone || isInvisible }?.let {
        animate()
            .translationX(0f)
            .setDuration(duration)
            .alpha(1f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    super.onAnimationStart(animation)
                    visibility = View.VISIBLE
                    alpha = 0f
                }
            })
    }
}

fun View.outToRight(duration: Long = 300) {
    takeIf { isVisible }?.let {
        animate()
            .translationX(width.toFloat())
            .setDuration(duration)
            .alpha(0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationStart(animation)
                    visibility = View.GONE
                }
            })
    }
}