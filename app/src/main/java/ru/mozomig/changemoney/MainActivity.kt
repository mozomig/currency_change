package ru.mozomig.changemoney

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import org.jetbrains.anko.setContentView
import ru.mozomig.changemoney.ui.container.ContainerUI
import ru.mozomig.changemoney.ui.currency.CurrencyFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ContainerUI().setContentView(this)

        val navHost = NavHostFragment.create(R.navigation.nav_graph)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, navHost)
            .setPrimaryNavigationFragment(navHost)
            .commit()
    }
}
