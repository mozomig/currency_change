package ru.mozomig.changemoney.model.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.mozomig.changemoney.model.dto.Gesmes
import ru.mozomig.changemoney.model.entity.CurrencyEntity
import ru.mozomig.changemoney.model.entity.Resource
import ru.mozomig.changemoney.model.services.CurrencyService
import java.math.BigDecimal


class CurrencyRepositoryImpl(app: Application) : CurrencyRepository {

    override val kodein: Kodein by org.kodein.di.android.kodein(app)
    private val service: CurrencyService  by instance()

    private var job: Deferred<Unit>? = null

    override fun get() = MutableLiveData<Resource<List<CurrencyEntity>>>().apply {
        value = Resource.loading()

        job?.cancel()

        job = GlobalScope.async(Dispatchers.IO) {
            while(true) {
                service.getCurrency().enqueue(object : Callback<Gesmes> {
                    override fun onFailure(call: Call<Gesmes>, t: Throwable) {
                        postValue(Resource.error(t.localizedMessage))
                    }

                    override fun onResponse(call: Call<Gesmes>, response: Response<Gesmes>) {
                        postValue(
                            Resource.success(
                                response.body()?.Cube?.cubes?.map {
                                    CurrencyEntity(
                                        currency = it.currency ?: "",
                                        rate = BigDecimal(it.rate)
                                    )
                                }
                            )
                        )
                    }
                })

                delay(30_000)
            }
        }
    }
}