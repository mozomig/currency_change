package ru.mozomig.changemoney.model.entity

data class Resource<T>(
    val status: Status,
    val data: T?,
    val error: String?
) {

    companion object {
        fun <T> success(data: T?) = Resource<T>(
            Status.SUCCESS,
            data,
            null
        )
        fun <T> error(ex: String?) = Resource<T>(
            Status.ERROR,
            null,
            ex
        )
        fun <T> loading() = Resource<T>(
            Status.LOADING,
            null,
            null
        )
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}