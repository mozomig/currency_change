package ru.mozomig.changemoney.model.entity

import androidx.annotation.IdRes
import java.math.BigDecimal

data class CurrencyEntity(
    val currency: String,
    val rate: BigDecimal
)