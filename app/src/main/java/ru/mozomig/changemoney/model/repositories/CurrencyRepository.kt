package ru.mozomig.changemoney.model.repositories

import androidx.lifecycle.LiveData
import org.kodein.di.KodeinAware
import ru.mozomig.changemoney.model.entity.CurrencyEntity
import ru.mozomig.changemoney.model.entity.Resource

interface CurrencyRepository: KodeinAware {

    fun get(): LiveData<Resource<List<CurrencyEntity>>>
}