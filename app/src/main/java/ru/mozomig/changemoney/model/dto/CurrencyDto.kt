package ru.mozomig.changemoney.model.dto

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root


@Root(name = "Envelope")
class Gesmes {

    @field:Element(name = "subject")
    var subject: String? = null

    @field:Element(name = "Sender")
    var sender: Sender? = null

    @field:Element(name = "Cube")
    var Cube: Cube? = null
}

@Root(name = "Cube")
class Cube {

    @field:ElementList(name = "Cube")
    var cubes: ArrayList<Cubes>? = null

    @field:Attribute(name = "time", required = false)
    var time: String? = null
}


@Root(name = "Cube")
class Cubes {

    @field:Attribute(name = "currency")
    var currency: String? = null
    @field:Attribute(name = "rate")
    var rate: String? = null
}

@Root(name = "Sender")
class Sender {
    @field:Element(name = "name")
    var name: String? = null
}