package ru.mozomig.changemoney.model.services

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import ru.mozomig.changemoney.model.dto.Gesmes

interface CurrencyService {
    @Headers("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3", "Accept-Encoding: gzip, deflate, br")
    @GET("stats/eurofxref/eurofxref-daily.xml")
    fun getCurrency(): Call<Gesmes>
}