package ru.mozomig.changemoney.ui.container

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent
import ru.mozomig.changemoney.R

class ContainerUI: AnkoComponent<AppCompatActivity> {
    override fun createView(ui: AnkoContext<AppCompatActivity>): View = with(ui) {
                frameLayout{
                    lparams(width = matchParent, height = matchParent)
                    id = R.id.container
                }
            }
}