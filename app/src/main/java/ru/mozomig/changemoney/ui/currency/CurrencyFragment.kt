package ru.mozomig.changemoney.ui.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import org.jetbrains.anko.AnkoContext
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.core.utils.getViewModel

class CurrencyFragment : Fragment() {
    private val vm by getViewModel<CurrencyViewModel>()
    private val ui by lazy { CurrencyUI(vm) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.load()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        ui.createView(AnkoContext.create(requireContext(), this, false))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(
            view.findViewById<Toolbar>(R.id.toolbar),
            findNavController()
        )
    }


}