package ru.mozomig.changemoney.ui.buttons

import android.content.Context
import android.os.Build
import android.widget.Button
import org.jetbrains.anko.allCaps
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.textColor
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.core.utils.correctColor
import ru.mozomig.changemoney.core.utils.correctDrawable

class PrimaryButton(ctx: Context): Button(ctx) {

    init {
        backgroundDrawable = correctDrawable(R.drawable.primary_button)
        allCaps = false
        textSize = 17f
        includeFontPadding = false
        textColor = correctColor(android.R.color.black)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            stateListAnimator = null
        }
    }
}