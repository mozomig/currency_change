package ru.mozomig.changemoney.ui.currency

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.model.entity.CurrencyEntity
import java.math.BigDecimal
import java.math.MathContext
import java.text.NumberFormat
import java.util.*

class CurrencyViewHolder(val item: View): RecyclerView.ViewHolder(item){
    fun bind(entity: CurrencyEntity) {
        item.findViewById<CurrencyItemView>(R.id.currency_item)?.run {
            title.text = entity.currency
            currency.text = NumberFormat.getNumberInstance().format(entity.rate.setScale(2, BigDecimal.ROUND_HALF_UP))
        }
    }
}