package ru.mozomig.greenplanet.ui.pulltorefresh

import android.content.Context
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import org.jetbrains.anko.*
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.core.utils.correctColor
import ru.mozomig.changemoney.core.utils.correctDrawable
import ru.mozomig.changemoney.ui.primaryButton

class ErrorView(ctx: Context) : _LinearLayout(ctx) {

    private val icon = imageView {
        id = R.id.error_icon
        scaleType = ImageView.ScaleType.CENTER_INSIDE
        setImageDrawable(correctDrawable(R.drawable.ic_sad_smile))
    }.lparams(width = dip(24), height = dip(24))

    private val errorView = textView {
        id = R.id.error_view
        textSize = 14f
        textColor = correctColor(R.color.grayText)
    }.lparams(width = wrapContent, height = wrapContent) {
        topMargin = dip(6)
    }

    var errorText = ""
        set(value) {
            field = value
            errorView.text = value
        }

    val retryBtn = primaryButton {
        id = R.id.retry
        setText(R.string.repeat)
        textSize = 13f
    }.lparams(width = dip(100), height = dip(40)) {
        topMargin = dip(10)
    }

    init {
        gravity = Gravity.CENTER
        orientation = LinearLayout.VERTICAL
    }
}