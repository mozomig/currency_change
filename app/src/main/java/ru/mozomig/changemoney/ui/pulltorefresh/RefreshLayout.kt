package ru.mozomig.greenplanet.ui.pulltorefresh

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.dinuscxj.refresh.RecyclerRefreshLayout
import org.jetbrains.anko.*
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.core.utils.correctColor
import ru.mozomig.changemoney.core.utils.dp2px
import ru.mozomig.changemoney.ui.errorView
import ru.mozomig.changemoney.ui.pulltorefresh.RefreshView
import ru.mozomig.changemoney.ui.refreshView

class RefreshLayout(ctx: Context) : RecyclerRefreshLayout(ctx) {
    private val refreshView = RefreshView(context)
    private var mOnRefreshListener: (() -> Unit)? = null
    private var state: State = State.FIRST_LOADING
    private val CONTANT_TAG = "CONTANT_TAG"
    private val LOADER_TAG = "LOADER_TAG"
    private val ERROR_TAG = "ERROR_TAG"

    private val errorView = _RelativeLayout(context).apply {
        id = R.id.error_view
        tag = ERROR_TAG
        lparams(width = matchParent, height = matchParent)
        errorView {
            id = R.id.error_wrapper
            retryBtn.setOnClickListener {
                setState(RefreshLayout.State.FIRST_LOADING)
                mOnRefreshListener?.invoke()
            }
        }.lparams(width = wrapContent, height = wrapContent) {
            centerInParent()
        }
    }

    private val firstLoading = _RelativeLayout(context).apply {
        id = R.id.first_loading
        tag = LOADER_TAG
        lparams(width = matchParent,height = matchParent)
        val loader = refreshView {
            id = R.id.refresh_view
        }.lparams(width = dip(24), height = dip(24)) {
            centerInParent()
        }

        textView(R.string.loading) {
            id = R.id.label_load
            textSize = 12f
            textColor = correctColor(R.color.grayText)
        }.lparams(width = wrapContent, height = wrapContent) {
            below(loader)
            topMargin = dip(4)
            centerHorizontally()
        }
    }

    var content: ViewGroup? = null
        set(value) {
            field = value
            value?.tag = CONTANT_TAG
        }

    init {
        setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED)
        setRefreshView(refreshView, ViewGroup.MarginLayoutParams(dip(24), dip(24)))
        setRefreshInitialOffset(dp2px(15f))
    }


    enum class State {
        SUCCESS,
        FIRST_LOADING,
        FAILED_LOADING;
    }

    fun setState(state: State) {
        this.state = state
        when (state) {
            State.SUCCESS -> enableRefresh()
            State.FIRST_LOADING -> firstLoading()
            State.FAILED_LOADING -> errorView()
        }
    }

    fun success() {
        state = State.SUCCESS
        enableRefresh()
    }

    fun error(text: String) {
        state = State.FAILED_LOADING
        errorView(text)
    }

    fun loading() {
        when (state) {
            State.FIRST_LOADING -> firstLoading()
        }
    }

    private fun enableRefresh() {
        chageLayout()
        findViewWithTag<View>(CONTANT_TAG)?.let {} ?: addView(content)

        setRefreshing(false)
        isEnabled = true
    }

    private fun firstLoading() {
        isEnabled = false
        chageLayout()
        findViewWithTag<View>(LOADER_TAG)?.let {} ?: addView(firstLoading.apply {
            findViewById<RefreshView>(R.id.refresh_view).refreshing()
        })
    }

    private fun errorView(text: String) {
        isEnabled = false
        chageLayout()
        findViewWithTag<View>(ERROR_TAG)?.let{} ?: addView(errorView.apply {
            findViewById<ErrorView>(R.id.error_wrapper).errorText = text
        })
    }

    private fun chageLayout() {
        when(state) {
            State.FIRST_LOADING -> {
                removeView(errorView)
                removeView(content)
            }
            State.FAILED_LOADING -> {
                removeView(firstLoading)
                removeView(content)
            }
            State.SUCCESS -> {
                removeView(firstLoading)
                removeView(errorView)
            }
        }
    }

    fun setOnRepeatListener(listener: () -> Unit) {
        mOnRefreshListener = listener
    }

    interface OnRepeatListener {
        fun onRepeat()
    }
}