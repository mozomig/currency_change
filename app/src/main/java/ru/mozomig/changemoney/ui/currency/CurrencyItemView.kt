package ru.mozomig.changemoney.ui.currency

import android.content.Context
import androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder
import org.jetbrains.anko.constraint.layout._ConstraintLayout
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.core.utils.correctColor
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.*

class CurrencyItemView(val ctx: Context) : _ConstraintLayout(ctx) {
    val title = textView {
        id = R.id.currency_item_title
        textSize = 14f
        textColor = correctColor(android.R.color.black)
    }.lparams(width = wrapContent, height = wrapContent)

    val currency = textView {
        id = R.id.currency_item_value
        textSize = 17f
        textColor = correctColor(R.color.grayText)
    }.lparams(width = wrapContent, height = wrapContent)

    init {

        lparams(width = wrapContent, height = wrapContent) {
            padding = dip(4)
        }

        applyConstraintSet {
            title {
                connect(
                    START to START of PARENT_ID,
                    END to END of PARENT_ID,
                    TOP to TOP of PARENT_ID,
                    BOTTOM to TOP of currency
                )
            }

            currency {
                connect(
                    START to START of PARENT_ID,
                    END to END of PARENT_ID,
                    TOP to BOTTOM of title,
                    BOTTOM to BOTTOM of PARENT_ID
                )
            }
        }
    }
}