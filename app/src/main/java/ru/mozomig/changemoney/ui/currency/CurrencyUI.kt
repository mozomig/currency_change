package ru.mozomig.changemoney.ui.currency

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.*
import ru.mozomig.changemoney.R
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.*
import ru.mozomig.changemoney.core.utils.*
import ru.mozomig.changemoney.model.entity.Resource
import ru.mozomig.changemoney.ui.*
import ru.mozomig.changemoney.ui.utils.DecimalFormatter
import java.math.BigDecimal

class CurrencyUI(private val vm: CurrencyViewModel) : AnkoComponent<Fragment> {
    private lateinit var ui: AnkoContext<Fragment>
    private lateinit var exchangeRecycle: RecyclerView
    private lateinit var currencyRateRecycle: RecyclerView

    override fun createView(ui: AnkoContext<Fragment>): View = with(ui) {
        this@CurrencyUI.ui = ui
        with(resources) {
            constraintLayout {
                val toolbar = appbarLayout {
                    id = R.id.currency_bar_layout

                    appToolbar {
                        id = R.id.toolbar
                        default()
                        lparams(width = matchParent, height = getAttr(android.R.attr.actionBarSize))
                    }
                }.lparams(width = matchConstraint, height = wrapContent)

                val content = refreshLayout {
                    id = R.id.refresh_layout

                    setOnRefreshListener {
                        vm.load()
                    }

                    setOnRepeatListener {
                        vm.load()
                    }

                    content = getContent(ui).lparams(width = matchParent, height = matchParent)

                    vm.exchangeRates.observe(ui.owner, Observer {
                        when (it.status) {
                            Resource.Status.SUCCESS -> {
                                it.data?.let { items -> (exchangeRecycle.adapter as CurrencyAdapter).update(items) }
                                success()
                            }
                            Resource.Status.LOADING -> loading()
                            Resource.Status.ERROR -> error(it.error ?: "")
                        }
                    })

                    vm.calculateRates.observe(ui.owner, Observer {
                        (currencyRateRecycle.adapter as CurrencyAdapter).update(it)
                    })

                }.lparams(width = matchConstraint, height = matchConstraint)

                applyConstraintSet {
                    toolbar {
                        connect(
                            START to START of PARENT_ID,
                            END to END of PARENT_ID,
                            TOP to TOP of PARENT_ID
                        )
                    }

                    content {
                        connect(
                            START to START of PARENT_ID,
                            END to END of PARENT_ID,
                            TOP to BOTTOM of toolbar,
                            BOTTOM to BOTTOM of PARENT_ID
                        )
                    }
                }
            }
        }
    }

    private fun getContent(ui: AnkoContext<Fragment>) = _ConstraintLayout(ui.ctx).apply {
        backgroundColor = correctColor(android.R.color.white)
        val ratesTitle = textView(R.string.currency) {
            id = View.generateViewId()
            textColor = correctColor(R.color.grayText)
            textSize = 14f
        }.lparams(width = matchConstraint, height = wrapContent)

        exchangeRecycle = recycleView {
            id = View.generateViewId()
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = CurrencyAdapter()
        }.lparams(width = matchConstraint, height = wrapContent)

        val sum = textInputLayout {
            id = R.id.sum_input
            isHintEnabled = true
            defaultStyle()

            textInputEditText {
                hint = resources.getString(R.string.enter_sum)

                inputType = InputType.TYPE_CLASS_PHONE
                keyListener = DigitsKeyListener.getInstance("0123456789 .")

                addTextChangedListener(object : TextWatcher {
                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                        this@textInputEditText.removeTextChangedListener(this)
                        this@textInputEditText.setText(DecimalFormatter().formatText(s.toString()))
                        this@textInputEditText.setSelection(text?.length ?: 0)
                        vm.setCurrency(text.toString().toSafeBigDecimal())
                        this@textInputEditText.addTextChangedListener(this)
                    }

                    override fun afterTextChanged(s: Editable) = Unit
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit
                })
            }
        }.lparams(width = matchConstraint, height = wrapContent)

        val currencyRate = textView(R.string.result) {
            id = View.generateViewId()
            textColor = correctColor(R.color.grayText)
            textSize = 14f
            gone()

            vm.currency.observe(ui.owner, Observer {
                inRoLShow(it > BigDecimal(0))
            })

        }.lparams(width = matchConstraint, height = wrapContent)

        currencyRateRecycle = recycleView {
            id = View.generateViewId()
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = CurrencyAdapter()
            gone()

            vm.currency.observe(ui.owner, Observer {
                inRoLShow(it > BigDecimal(0))
            })
        }.lparams(width = matchConstraint, height = wrapContent)

        applyConstraintSet {
            ratesTitle {
                connect(
                    START to START of PARENT_ID margin dip(16),
                    END to END of PARENT_ID margin dip(16),
                    TOP to TOP of PARENT_ID margin dip(8)
                )
            }

            exchangeRecycle {
                connect(
                    START to START of PARENT_ID margin dip(12),
                    END to END of PARENT_ID margin dip(12),
                    TOP to BOTTOM of ratesTitle
                )
            }

            sum {
                connect(
                    START to START of PARENT_ID margin dip(16),
                    END to END of PARENT_ID margin dip(16),
                    TOP to BOTTOM of exchangeRecycle margin dip(8)
                )
            }

            currencyRate {
                connect(
                    START to START of PARENT_ID margin dip(16),
                    END to END of PARENT_ID margin dip(16),
                    TOP to BOTTOM of sum margin dip(8)
                )
            }

            currencyRateRecycle {
                connect(
                    START to START of PARENT_ID margin dip(16),
                    END to END of PARENT_ID margin dip(16),
                    TOP to BOTTOM of currencyRate
                )
            }
        }
    }
}