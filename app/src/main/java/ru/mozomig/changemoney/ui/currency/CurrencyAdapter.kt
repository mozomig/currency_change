package ru.mozomig.changemoney.ui.currency

import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.dip
import org.jetbrains.anko.margin
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.wrapContent
import ru.mozomig.changemoney.R
import ru.mozomig.changemoney.model.entity.CurrencyEntity
import ru.mozomig.changemoney.ui.currencyItemView

class CurrencyAdapter : RecyclerView.Adapter<CurrencyViewHolder>() {
    private var items = listOf<CurrencyEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder =
        CurrencyViewHolder(CardView(parent.context).apply {
            layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, dip(60)).apply {
                margin = dip(4)
            }
            currencyItemView {
                id = R.id.currency_item
                lparams(width = wrapContent, height = matchParent)
            }
        })

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) = holder.bind(items[position])

    fun update(items: List<CurrencyEntity>) {
        this.items = items
        notifyDataSetChanged()
    }
}

