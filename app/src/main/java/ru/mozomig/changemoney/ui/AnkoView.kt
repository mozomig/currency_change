package ru.mozomig.changemoney.ui

import android.view.ViewManager
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.jetbrains.anko.custom.ankoView
import ru.mozomig.changemoney.ui.buttons.PrimaryButton
import ru.mozomig.changemoney.ui.currency.CurrencyItemView
import ru.mozomig.changemoney.ui.pulltorefresh.RefreshView
import ru.mozomig.greenplanet.ui.pulltorefresh.ErrorView
import ru.mozomig.greenplanet.ui.pulltorefresh.RefreshLayout

inline fun ViewManager.appbarLayout() = appbarLayout() {}
inline fun ViewManager.appbarLayout(init: AppBarLayout.() -> Unit) =
    ankoView({ AppBarLayout(it) }, 0, init)

inline fun ViewManager.appToolbar() = appToolbar() {}
inline fun ViewManager.appToolbar(init: Toolbar.() -> Unit) =
    ankoView({ Toolbar(it) }, 0, init)

inline fun ViewManager.primaryButton() = primaryButton() {}
inline fun ViewManager.primaryButton(init: PrimaryButton.() -> Unit) =
    ankoView({ PrimaryButton(it) }, 0, init)

inline fun ViewManager.refreshLayout() = refreshLayout {}
inline fun ViewManager.refreshLayout(init: RefreshLayout.() -> Unit) =
    ankoView({ RefreshLayout(it) }, 0, init)

inline fun ViewManager.errorView() = errorView {}
inline fun ViewManager.errorView(init: ErrorView.() -> Unit) =
    ankoView({ ErrorView(it) }, 0, init)

inline fun ViewManager.refreshView() = refreshView {}
inline fun ViewManager.refreshView(init: RefreshView.() -> Unit) =
    ankoView({ RefreshView(it) }, 0, init)

inline fun ViewManager.recycleView() = recycleView {}
inline fun ViewManager.recycleView(init: RecyclerView.() -> Unit) =
    ankoView({ RecyclerView(it) }, 0, init)

inline fun ViewManager.textInputLayout() = textInputLayout {}
inline fun ViewManager.textInputLayout(init: TextInputLayout.() -> Unit) =
    ankoView({ TextInputLayout(it) }, 0, init)

inline fun ViewManager.textInputEditText() = textInputEditText() {}
inline fun ViewManager.textInputEditText(init: TextInputEditText.() -> Unit) =
    ankoView({ TextInputEditText(it) }, 0, init)

inline fun ViewManager.currencyItemView() = currencyItemView() {}
inline fun ViewManager.currencyItemView(init: CurrencyItemView.() -> Unit) =
    ankoView({ CurrencyItemView(it) }, 0, init)
