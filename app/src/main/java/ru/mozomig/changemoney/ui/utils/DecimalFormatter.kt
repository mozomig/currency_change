package ru.mozomig.changemoney.ui.utils

import ru.mozomig.changemoney.core.utils.parseBigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.regex.Pattern

class DecimalFormatter : DecimalFormat() {
    init {
        maximumFractionDigits = 2
        minimumFractionDigits = 0
        isGroupingUsed = true
        groupingSize = 3
        decimalFormatSymbols = DecimalFormatSymbols().apply {
            decimalSeparator = '.'
            groupingSeparator = SEPARATOR_GROUP
        }
        roundingMode = RoundingMode.DOWN
    }

    fun formatText(cleanText: String): String {
        return if (amountPattern.matcher(cleanText).matches()) cleanText else cleanText(cleanText)
    }

    fun cleanText(formattedText: String): String {
        val decimal = formattedText.parseBigDecimal()
        return if (decimal == null) "" else format(decimal)
    }

    companion object {
        private const val SEPARATOR_GROUP = ' '

        private val amountPattern: Pattern
            get() = Pattern.compile("^\\d{1,3}( \\d{3})*(\\.\\d{0,2})?")
    }
}