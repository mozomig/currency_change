package ru.mozomig.changemoney.ui.currency

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import ru.mozomig.changemoney.model.entity.CurrencyEntity
import ru.mozomig.changemoney.model.entity.Resource
import ru.mozomig.changemoney.model.repositories.CurrencyRepository
import java.math.BigDecimal

class CurrencyViewModel(app: Application) : AndroidViewModel(app), KodeinAware {
    override val kodein: Kodein by org.kodein.di.android.kodein(app)
    private val currencyRepo: CurrencyRepository by instance()

    private val _load = MutableLiveData<Boolean>()

    val currency: LiveData<BigDecimal> = MutableLiveData<BigDecimal>()

    val exchangeRates: LiveData<Resource<List<CurrencyEntity>>> = Transformations.switchMap(_load) {
        Transformations.map(currencyRepo.get()) {
            Resource(it.status, it.data, it.error)
        }
    }

    val calculateRates: LiveData<List<CurrencyEntity>> = Transformations.switchMap(currency) {
        calculateRates(it)
    }

    fun load() {
        _load.value = true
    }

    fun setCurrency(sum: BigDecimal) {
        (currency as MutableLiveData).value = sum
    }

    private fun calculateRates(sum: BigDecimal) = MutableLiveData<List<CurrencyEntity>>().apply {
        GlobalScope.launch {
            with(mutableListOf<CurrencyEntity>()) {
                exchangeRates.value?.data?.forEach {
                    add(
                        CurrencyEntity(
                            currency = it.currency,
                            rate = sum * it.rate
                        )
                    )
                }

                postValue(this)
            }
        }
    }
}